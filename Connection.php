<?php

class Connection
{
    /**
     * @var \SQLite3
     */
    private $db;
    private $default = "true";

    public function __construct()
    {
        $migrate = !file_exists($_SERVER['DOCUMENT_ROOT'] . '/table.sqlite');
        $this->db = new SQLite3($_SERVER['DOCUMENT_ROOT'] . '/table.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);
        if ($migrate) {
            echo "migrating";
            $this->db->query('CREATE TABLE IF NOT EXISTS "questions" (
                "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                "q" VARCHAR UNIQUE,
                "question" VARCHAR
            )');
        }
    }

    public function get(string $id): string
    {
        preg_replace('/[^a-zA-Z]/', '', $id);

        $res = $this->db->querySingle('SELECT * FROM "questions" WHERE "q" = \'' . $id . '\'', true);
        if ($res) {
            return $res['question'];
        }
        return $this->default;
    }

    public function create($string): string
    {
        $id = $this->generateId();
        while($this->get($id) != $this->default) {
            $id = $this->generateId();
        }
        $this->insert($id, $string);
        return $id;
    }

    private function generateId(): string
    {
        return substr(bin2hex(random_bytes(20)), 0, 3);
    }

    private function insert($id, $question)
    {
        $statement = $this->db->prepare('INSERT INTO "questions" ("q", "question") VALUES (:q, :question)');
        $statement->bindValue(':q', $id);
        $statement->bindValue(':question', $question);
        $statement->execute();
    }
}
