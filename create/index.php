<?php
if (isset($_POST['question'])) {
    include($_SERVER['DOCUMENT_ROOT'] . '/Connection.php');
    $res = new Connection();
    $id = $res->create($_POST['question']);
    header('Location: /?q='.$id);
    exit();
}
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Let's sell a bridge...</title>
</head>
<body>
    <h2>Let's sell a bridge...</h2>
    <form method="post">
        <label for="question">You think that's </label><input id="question" name="question" type="text" placeholder="true"><span>?</span>
        <br />
        <br />
        <span>Well I've got a bridge to sell you!</span>
        <br />
        <br />
        <input type="submit">
    </form>
<a href="https://gitlab.com/blakethepatton/bridge-to-sell" style="position: absolute; bottom: 0; right:0; padding: 5px;">Source</a>
</body>
</html>
