<?php
$question = 'true';
if (isset($_GET['q'])) {
    include('./Connection.php');
    $res = new Connection();
    $question = $res->get($_GET['q']);
}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Well I've got a bridge...</title>
</head>
<body>
<pre>
    You think that's <?php echo $question; ?>?


                               ___....___
     ^^                __..-:'':__:..:__:'':-..__
                   _.-:__:.-:'':  :  :  :'':-.:__:-._
                 .':.-:  :  :  :  :  :  :  :  :  :._:'.
              _ :.':  :  :  :  :  :  :  :  :  :  :  :'.: _
             [ ]:  :  :  :  :  :  :  :  :  :  :  :  :  :[ ]
             [ ]:  :  :  :  :  :  :  :  :  :  :  :  :  :[ ]
    :::::::::[ ]:__:__:__:__:__:__:__:__:__:__:__:__:__:[ ]:::::::::::
    !!!!!!!!![ ]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!![ ]!!!!!!!!!!!
    ^^^^^^^^^[ ]^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^[ ]^^^^^^^^^^^
             [ ]                                        [ ]
             [ ]                                        [ ]
       jgs   [ ]                                        [ ]
     ~~^_~^~/   \~^-~^~ _~^-~_^~-^~_^~~-^~_~^~-~_~-^~_^/   \~^ ~~_ ^


    Well I've got a bridge to sell you! And because we're pals, I'll even give you a discount!
</pre>
<a href="/create" style="position: absolute; bottom: 0; right:0; padding: 5px;">Make your own</a>
</body>
</html>
